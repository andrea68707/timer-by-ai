import tkinter as tk
import pygame

class TimerApp:
    def __init__(self, root):
        self.root = root
        self.root.title("Timer App")

        self.minutes = tk.StringVar()
        self.seconds = tk.StringVar()
        self.total_seconds = 0
        self.timer_paused = False

        self.entry = tk.Entry(root, textvariable=self.minutes, width=3)
        self.entry.pack(side=tk.LEFT, padx=5)
        self.label2 = tk.Label(root, text=":")
        self.label2.pack(side=tk.LEFT, padx=5)
        self.entry2 = tk.Entry(root, textvariable=self.seconds, width=3)
        self.entry2.pack(side=tk.LEFT, padx=5)

        self.set_button = tk.Button(root, text="Set Timer", command=self.set_timer)
        self.set_button.pack(pady=10)

        self.timer_label = tk.Label(root, text="00:00")
        self.timer_label.pack(padx=5, pady=10)

        self.start_button = tk.Button(root, text="Start Timer", command=self.start_timer)
        self.start_button.pack(padx=5)

        self.pause_button = tk.Button(root, text="Pause Timer", command=self.pause_timer)
        self.pause_button.pack(padx=5)

        self.resume_button = tk.Button(root, text="Resume Timer", command=self.resume_timer)
        self.resume_button.pack(padx=5)

        self.stop_button = tk.Button(root, text="Stop Timer", command=self.stop_timer)
        self.stop_button.pack(padx=5)

        self.timer_id = None

        self.root.protocol("WM_DELETE_WINDOW", self.on_closing)

    def set_timer(self):
        try:
            minutes = int(self.minutes.get())
            seconds = int(self.seconds.get())
            self.total_seconds = minutes * 60 + seconds
            if self.total_seconds <= 0:
                print("Please enter a valid time")
            else:
                self.timer_label.config(text=f"{minutes:02d}:{seconds:02d}")
        except ValueError:
            print("Please enter valid numbers")

    def start_timer(self):
        if self.total_seconds == 0:
            print("Please set the timer first")
            return
        self.timer_id = self.timer_label.after(1000, self.update_timer)

    def update_timer(self):
        if self.total_seconds > 0 and not self.timer_paused:
            self.total_seconds -= 1
            minutes = self.total_seconds // 60
            seconds = self.total_seconds % 60
            self.timer_label.config(text=f"{minutes:02d}:{seconds:02d}")
            self.timer_id = self.timer_label.after(1000, self.update_timer)
        elif self.total_seconds == 0:
            print("Timer completed!")
            sound_file_path = r"-" # Insert the desired path to mp3 file
            self.play_sound(sound_file_path)  # Play the alarm sound when the timer ends

    def play_sound(self, sound_file):
        try:
            pygame.mixer.init()
            pygame.mixer.music.load(sound_file)
            pygame.mixer.music.play()
        except Exception as e:
            print(f"Error playing sound: {e}")

    def pause_timer(self):
        self.timer_paused = True

    def resume_timer(self):
        if self.timer_paused:
            self.timer_paused = False
            self.timer_id = self.timer_label.after(1000, self.update_timer)

    def stop_timer(self):
        if self.timer_id is not None:
            self.root.after_cancel(self.timer_id)
            self.timer_label.config(text="00:00")
            self.total_seconds = 0
            self.timer_paused = False
            print("Timer stopped")

    def on_closing(self):
        self.root.destroy()

root = tk.Tk()
app = TimerApp(root)
root.mainloop()