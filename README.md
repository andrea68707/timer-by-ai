# timer-by-ai

A very simple python script created within 7 to 10 minutes, entirely made by Codeium AI (Base Version).

The script launches a basic timer that can be set, started, paused, resumed and stopped. It also plays an audio file when the timer ends.
(The audio file must be configured by inserting the path to a given .mp3 file on the system)